import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/components/Dashboard'
import Page2 from '@/components/Page2'
import Page3 from '@/components/Page3'
import Page4 from '@/components/Page4'
import Page5 from '@/components/Page5'

Vue.use(Router)

export default new Router({
	routes:[
		{
			path: '/dashboard',
			name: 'Dashboard',
			component: Dashboard
		},
		{
			path: '/page2',
			name: 'Page2',
			component: Page2
		},
		{
			path: '/page3',
			name: 'Page3',
			component: Page3
		},
		{
			path: '/page4',
			name: 'Page4',
			component: Page4
		},
		{
			path: '/page5',
			name: 'Page5',
			component: Page5
		}
	]
})